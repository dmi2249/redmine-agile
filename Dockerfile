FROM redmine:latest
COPY ./data/database.yml /usr/src/redmine/config/database.yml
COPY ./data/themes /usr/src/redmine/public/themes
COPY ./data/plugins /usr/src/redmine/plugins

# Строки закоментированы, т.к. при выполнении последней команды возникает ошибка ArgumentError: Missing `secret_key_base` for 'production' environment, set this string with `rails credentials:edit`
#WORKDIR  /usr/src/redmine 
#RUN bundle install --without development test
#RUN bundle exec rake redmine:plugins NAME=redmine_agile RAILS_ENV=production	

EXPOSE 3000
