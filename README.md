# redmine-agile
В основе образ redmine:latest из dockerhub

# Добавлена тема.
Тема не требует дополнительной настройки, её необходимо включить в Администрирование - Настройки - Отображение

# Добавлен плагин
Добавлен плагин, позволяющий отображать Agile доску.
Для активации плагина требуется подключиться к bash контейнера и выполнить команды:

<code>bundle install --without development test</code>

<code>bundle exec rake redmine:plugins NAME=redmine_agile RAILS_ENV=production</code>


Пытался добавить выполнение команд при создании контейнера, но возникает ошибка

<code>ArgumentError: Missing `secret_key_base` for 'production' environment, set this string with `rails credentials:edit`</code>